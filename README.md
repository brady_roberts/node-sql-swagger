# node-sql-swagger

Generate Swagger 2.0 interface CRUD interface and implementations for postgres database.

The idea is based on [sql-generate](https://www.npmjs.com/package/sql-generate) from which I started and then extended it to do a bunch of very specific things to generate CRUD based on my peculiar habits with laying out database schemas.
